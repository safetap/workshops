 /***************************************************
NAYAD HOME KIT v0.1 - Example 5 (Access Point Webserver)

 GNU Lesser General Public License.
 See <http://www.gnu.org/licenses/> for details.
 ****************************************************/
 
 /***********Calibration CMD***************
     enter -> enter the calibration mode
     cal:tds value -> calibrate with the known tds value(25^c). e.g.cal:707
     exit -> save the parameters and exit the calibration mode
 ****************************************************/

#include <WiFi.h>
#include <ESPAsyncWebServer.h>
#include <SPIFFS.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <Wire.h>
#include "GravityTDS.h"
#include <EEPROM.h>

// Replace with your network credentials
const char* ssid   = "NAYAD_MODULAR";
const char* password = "1234567890";

// Set web server port number to 80
WiFiServer server(80);

// Variable to store the HTTP request
String header;


//TEMPERATURE SETUP. Using GPIO where the Temperature sensor is connected to.

const int oneWireBus = 0;

OneWire oneWire (oneWireBus);

DallasTemperature sensors (&oneWire);

String temperature() {
  // Read temperature as Celsius (the default)
  sensors.requestTemperatures();
  float temperature = sensors.getTempCByIndex(0);
  sensors.requestTemperatures();
  if (isnan(temperature)) {
    Serial.println("Failed to read from Sensor1!");
    return "";
  }
  else {
    Serial.println(temperature);
    return String(temperature);
  }
}


//TDS SETUP
#define TdsSensorPin 35
GravityTDS gravityTds;

float tdsValue = 25;



String TDS() {
  sensors.requestTemperatures();
  float temperature = sensors.getTempCByIndex(0);
  gravityTds.setTemperature(temperature);  // set the temperature and execute temperature compensation
  gravityTds.update();  //sample and calculate
  tdsValue = gravityTds.getTdsValue();  // then get the value

  if (isnan(tdsValue)) {
    Serial.println("Failed to read from Sensor!");
    return "";
  }
  else {
    Serial.println(tdsValue);
    return String(tdsValue);
  }
}

// Set up the rgb led
uint8_t ledR = 5;
uint8_t ledG = 2;
uint8_t ledB = 4;


void setup() {
  Serial.begin(115200);
  // Initialize the output variables as outputs
  sensors.begin();
  sensors.setResolution(0);

  EEPROM.begin(13);//needed EEPROM.begin to store calibration k in eeprom
  gravityTds.begin();  //initialization

  ledcAttachPin(ledR, 1);
  ledcAttachPin(ledG, 2);
  ledcAttachPin(ledB, 3);
  ledcSetup(1, 12000, 8);
  ledcSetup(2, 12000, 8);
  ledcSetup(3, 12000, 8);
  ledcWrite(1, 256);
  ledcWrite(2, 256);
  ledcWrite(3, 256);
  delay(1000);

  // Connect to Wi-Fi network with SSID and password
  Serial.print("Setting AP (Access Point)…");
  // Remove the password parameter, if you want the AP (Access Point) to be open
  WiFi.softAP(ssid, password);

  IPAddress IP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(IP);

  server.begin();
}





void loop() {
  sensors.requestTemperatures();
  float temperatureA = sensors.getTempCByIndex(0);

  gravityTds.setTemperature(temperatureA);  // set the temperature and execute temperature compensation
  gravityTds.update();  //sample and calculate
  tdsValue = gravityTds.getTdsValue();  // then get the value

  Serial.print("Temperature Censius: ");
  Serial.print(temperatureA);
  Serial.println("ºC");
  Serial.print("TDS: ");
  Serial.print(tdsValue, 0);
  Serial.println("ppm");
  delay(1000);

  // If your RGB LED turns off instead of on here you should check if the LED is common anode or cathode.
  // If it doesn't fully turn off and is common anode try using 256.
  if ((tdsValue > 300) && (tdsValue < 500)) {
    ledcWrite(1, 0);
    ledcWrite(2, 150);
    ledcWrite(3, 200);
  }
  if (tdsValue < 300) {
    ledcWrite(1, 0);
    ledcWrite(2, 256);
    ledcWrite(3, 0);

  }
  if (tdsValue >= 500) {
    ledcWrite(1, 0);
    ledcWrite(2, 0);
    ledcWrite(3, 256);
  }




  WiFiClient client = server.available();   // Listen for incoming clients

  if (client)
  {
    Serial.println("Web Client connected ");
    String request = client.readStringUntil('\r');
    client.println("HTTP/1.1 200 OK");
    client.println("Content-type:text/html");
    client.println("Connection: close");
    client.println();
    client.println("<!DOCTYPE html><html>");
    client.println("<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
    client.println("<link rel=\"icon\" href=\"data:,\">");

    client.println("<style>body { text-align: center; font-family: \"Arial\", Arial;}");
    client.println("table { border-collapse: collapse; width:40%; margin-left:auto; margin-right:auto;border-spacing: 2px;background-color: white;border: 4px solid green; }");
    client.println("th { padding: 20px; background-color: #2660ff; color: white; }");
    client.println("tr { border: 5px solid blue; padding: 2px; }");
    client.println("tr:hover { background-color:yellow; }");
    client.println("td { border:4px; padding: 12px; }");
    client.println(".sensor { color:white; font-weight: bold; background-color: #bcbcbc; padding: 1px; }");

    client.println("</style></head><body><h1>NAYAD Access Point</h1>");
    client.println("<h2>Access Point Mode</h2>");
    client.println("<h2>hackingecology.com/hardware-and-product/</h2>");
    client.println("<table><tr><th>MEASUREMENT</th><th>VALUE</th></tr>");
    client.println("<tr><td>Temperature</td><td><span class=\"sensor\">");
    client.println(temperature());
    client.println(" *C</span></td></tr>");
    client.println("<tr><td>TDS</td><td><span class=\"sensor\">");
    client.println(TDS());
    client.println(" ppm</span></td></tr>");
    client.println("</body></html>");
    client.stop();
    client.println();
    Serial.println("Client disconnected.");
    Serial.println("");
  }

  delay(2000);

}
