 /***************************************************
NAYAD HOME KIT v0.1 - Example 2 (RGBLED)

 GNU Lesser General Public License.
 See <http://www.gnu.org/licenses/> for details.
 ****************************************************/
 
 /***********Calibration CMD***************
     enter -> enter the calibration mode
     cal:tds value -> calibrate with the known tds value(25^c). e.g.cal:707
     exit -> save the parameters and exit the calibration mode
 ****************************************************/
#include <Arduino.h>
#include <EEPROM.h>
#include "GravityTDS.h"
#include <WiFi.h>


#define TdsSensorPin 35 //11 at nayad
GravityTDS gravityTds;

float temperature = 25,tdsValue = 0;

// Set up the rgb led
uint8_t ledR = 5;
uint8_t ledG = 2;
uint8_t ledB = 4; 


void setup()
{
    Serial.begin(115200);
    gravityTds.setPin(TdsSensorPin);
    gravityTds.setAref(3.0);  //reference voltage on ADC, default 5.0V on Arduino UNO
    gravityTds.setAdcRange(4096);  //1024 for 10bit ADC;4096 for 12bit ADC
    gravityTds.begin();  //initialization
    EEPROM.begin(32);//needed EEPROM.begin to store calibration k in eeprom
    
    ledcAttachPin(ledR, 1);
    ledcAttachPin(ledG, 2);
    ledcAttachPin(ledB, 3);
    ledcSetup(1, 12000, 8);
    ledcSetup(2, 12000, 8);
    ledcSetup(3, 12000, 8);
    ledcWrite(1, 256);
    ledcWrite(2, 256);
    ledcWrite(3, 256);
    delay(1000);

}

void loop()
{
    //temperature = readTemperature();  //add your temperature sensor and read it
    gravityTds.setTemperature(temperature);  // set the temperature and execute temperature compensation
    gravityTds.update();  //sample and calculate 
    tdsValue = gravityTds.getTdsValue();  // then get the value
    Serial.print(tdsValue,0);
    Serial.println("ppm");
    delay(1000);

    // If your RGB LED turns off instead of on here you should check if the LED is common anode or cathode.
    // If it doesn't fully turn off and is common anode try using 256.
    if ((tdsValue > 300)&& (tdsValue < 500)){
      ledcWrite(1, 0);
      ledcWrite(2, 150);
      ledcWrite(3, 200);
    }
    if (tdsValue < 300){
      ledcWrite(1, 0);
      ledcWrite(2, 256);
      ledcWrite(3, 0);
      
    }
    if (tdsValue >=500){
      ledcWrite(1, 0);
      ledcWrite(2, 0);
      ledcWrite(3, 256);
    }
}
