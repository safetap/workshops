 /***************************************************
NAYAD HOME KIT v0.1 - Example 4 (Sensing Platform)

 GNU Lesser General Public License.
 See <http://www.gnu.org/licenses/> for details.
 ****************************************************/
 
 /***********Calibration CMD***************
     enter -> enter the calibration mode
     cal:tds value -> calibrate with the known tds value(25^c). e.g.cal:707
     exit -> save the parameters and exit the calibration mode
 ****************************************************/
#include <Arduino.h>
#include <EEPROM.h>
#include "GravityTDS.h"
#include <OneWire.h>
#include <DallasTemperature.h>
#include <ThingsBoard.h>
#include <ArduinoJson.h>
#include <Wire.h>
#include <WiFi.h>


// CLOUD SETUP
#define WIFI_AP "vodafoneB9E8"
#define WIFI_PASSWORD "2dWPgwtT@$"
#define TOKEN "lPcGCe2OfBV2T3vAPVFB"
char thingsboardServer[] = "sensing.hackingecology.com";

//WiFi SETUP
WiFiClient wifiClient;
ThingsBoard hesensing(wifiClient);
int status = WL_IDLE_STATUS;
unsigned long lastSend;


const int oneWireBus = 0;
OneWire oneWire (oneWireBus);
DallasTemperature sensors (&oneWire);


#define TdsSensorPin 35 //11 at nayad
GravityTDS gravityTds;

float tdsValue = 0;

// Set up the rgb led
uint8_t ledR = 5;
uint8_t ledG = 2;
uint8_t ledB = 4; 


void InitWiFi()
{
    Serial.println("Connecting to AP ...");
    // attempt to connect to WiFi network
    
    WiFi.begin(WIFI_AP, WIFI_PASSWORD);
    while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.print(".");
    }
    Serial.println("Connected to AP");
}


void getAndSendData()
{
    Serial.println("Collecting Nayad data.");
    
    sensors.requestTemperatures(); 
    float temperatureC = sensors.getTempCByIndex(0);
    float temperatureF = sensors.getTempFByIndex(0);
    
    gravityTds.setTemperature(temperatureC);  // set the temperature and execute temperature compensation
    gravityTds.update();  //sample and calculate 
    tdsValue = gravityTds.getTdsValue();  // then get the value
    
    // Check if any reads failed and exit early (to try again).
    if (isnan(temperatureC) || isnan(temperatureF)|| isnan(tdsValue)) {
      Serial.println("Failed to read from sensor!");
      return;
    }
    
    hesensing.sendTelemetryFloat("Temperature Censius:", temperatureC);
    hesensing.sendTelemetryFloat("Temperature Farenheit:", temperatureF);
    hesensing.sendTelemetryFloat("TDS:", tdsValue);
    }


void reconnect() {
    // Loop until we're reconnected
    while (!hesensing .connected()) {
      status = WiFi.status();
      if ( status != WL_CONNECTED) {
        WiFi.begin(WIFI_AP, WIFI_PASSWORD);
        while (WiFi.status() != WL_CONNECTED) {
          delay(500);
          Serial.print(".");
        }
        Serial.println("Connected to AP");
      }
      Serial.print("Connecting to ThingsBoard node ...");
      if (hesensing.connect(thingsboardServer, TOKEN) ) {
        Serial.println( "[DONE]" );
      } else {
        Serial.print( "[FAILED]" );
        Serial.println( " : retrying in 5 seconds]" );
        // Wait 5 seconds before retrying
        delay( 5000 );
      }
    }
}


void setup()
{
    Serial.begin(115200);
    sensors.begin();
    sensors.setResolution(0);
    
    gravityTds.setPin(TdsSensorPin);
    gravityTds.setAref(3.0);  //reference voltage on ADC, default 5.0V on Arduino UNO
    gravityTds.setAdcRange(4096);  //1024 for 10bit ADC;4096 for 12bit ADC
    gravityTds.begin();  //initialization
    EEPROM.begin(32);//needed EEPROM.begin to store calibration k in eeprom
    delay(10);
    InitWiFi();
    lastSend = 0;
    
    ledcAttachPin(ledR, 1);
    ledcAttachPin(ledG, 2);
    ledcAttachPin(ledB, 3);
    ledcSetup(1, 12000, 8);
    ledcSetup(2, 12000, 8);
    ledcSetup(3, 12000, 8);
    ledcWrite(1, 256);
    ledcWrite(2, 256);
    ledcWrite(3, 256);
    delay(1000);

}

void loop()
{
    sensors.requestTemperatures(); 
    float temperatureC = sensors.getTempCByIndex(0);
    float temperatureF = sensors.getTempFByIndex(0);
    
    gravityTds.setTemperature(temperatureC);  // set the temperature and execute temperature compensation
    gravityTds.update();  //sample and calculate 
    tdsValue = gravityTds.getTdsValue();  // then get the value

    Serial.print("Temperature Censius: ");
    Serial.print(temperatureC);
    Serial.println("ºC");
    Serial.print("TDS: ");
    Serial.print(tdsValue,0);
    Serial.println("ppm");
    delay(1000);

    // If your RGB LED turns off instead of on here you should check if the LED is common anode or cathode.
    // If it doesn't fully turn off and is common anode try using 256.
    if ((tdsValue > 300)&& (tdsValue < 500)){
      ledcWrite(1, 0);
      ledcWrite(2, 150);
      ledcWrite(3, 200);
    }
    if (tdsValue < 300){
      ledcWrite(1, 0);
      ledcWrite(2, 256);
      ledcWrite(3, 0);
      
    }
    if (tdsValue >=500){
      ledcWrite(1, 0);
      ledcWrite(2, 0);
      ledcWrite(3, 256);
    }

    if ( !hesensing.connected() ) {
      reconnect();
    }
  
    if ( millis() - lastSend > 1000 ) { // Update and send only after 1 seconds
      getAndSendData();
      lastSend = millis();
    }
  
    hesensing.loop();
}
