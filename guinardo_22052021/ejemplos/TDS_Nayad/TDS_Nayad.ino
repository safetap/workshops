/***************************************************
NAYAD HOME KIT v0.1 - Example 1 (TDS Sensor + Simulated temperature)

 GNU Lesser General Public License.
 See <http://www.gnu.org/licenses/> for details.
 ****************************************************/
  
 /***********Calibration CMD***************
     enter -> enter the calibration mode
     cal:tds value -> calibrate with the known tds value(25^c). e.g.cal:707
     exit -> save the parameters and exit the calibration mode
 ****************************************************/
#include <Arduino.h>
#include <EEPROM.h>
#include "GravityTDS.h"
#include <WiFi.h>


#define TdsSensorPin 35 //11 at nayad
GravityTDS gravityTds;

float temperature = 25,tdsValue = 0;


void setup()
{
    Serial.begin(115200);
    gravityTds.setPin(TdsSensorPin);
    gravityTds.setAref(3.0);  //reference voltage on ADC, default 5.0V on Arduino UNO
    gravityTds.setAdcRange(4096);  //1024 for 10bit ADC;4096 for 12bit ADC
    gravityTds.begin();  //initialization
    EEPROM.begin(32);//needed EEPROM.begin to store calibration k in eeprom

}

void loop()
{
    //temperature = readTemperature();  //add your temperature sensor and read it
    gravityTds.setTemperature(temperature);  // set the temperature and execute temperature compensation
    gravityTds.update();  //sample and calculate 
    tdsValue = gravityTds.getTdsValue();  // then get the value
    Serial.print(tdsValue,0);
    Serial.println("ppm");
    delay(1000);
}
